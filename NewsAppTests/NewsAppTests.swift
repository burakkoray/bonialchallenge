//
//  ChallengeTests.swift
//  ChallengeTests
//
//  Created by Burak Koray Kose on 9.11.2019.
//  Copyright © 2019 Burak Koray Kose. All rights reserved.
//

import XCTest
@testable import NewsApp

class ChallengeTests: XCTestCase {
    
    private var view: MockHeadlinesView!
    private var interactor: TopHeadlinesInteractor!
    private var presenter: TopHeadlinesPresenter!
    private var router: MockHeadlinesRouter!
    private var service: MockNewsService!
    
    override func setUp() {
        service = MockNewsService()
        
        view = MockHeadlinesView()
        interactor = TopHeadlinesInteractor(service: service)
        router = MockHeadlinesRouter()
        presenter = TopHeadlinesPresenter(view: view,
                                          interactor: interactor,
                                          router: router)
        
        view.presenter = presenter
    }
    
    func testLoadHeadlines(){
        // Given:
        let headlines = try? ResourceLoader.loadHeadlines()
        service.topHeadlines = headlines
        
        // When:
        view.viewDidLoad()
        
        // Then:
        XCTAssertEqual(view.outputs.count, 3)
        
        XCTAssertEqual(try view.outputs[0], .setLoading(true))
        XCTAssertEqual(try view.outputs[1], .setLoading(false))
        let expectedArticles = service.topHeadlines!.articles.map(ArticlePresentation.init)
        XCTAssertEqual(try view.outputs[2], TopHeadlinesPresenterOutput.showArticleList(expectedArticles))
    }
    
    func testSelectArticle() throws {
        // Given:
        let headlines = try? ResourceLoader.loadHeadlines()
        service.topHeadlines = headlines
        view.viewDidLoad()
        
        // When:
        view.selectArticle(at: 4)
        
        //Then:
        let expectedArticle = try service.topHeadlines?.articles[4]
        XCTAssertEqual(try router.routes[0], .detail(expectedArticle!))
    }
}

private final class MockHeadlinesView: TopHeadlinesViewProtocol {
    
    var presenter: TopHeadlinesPresenter!
    var outputs: [TopHeadlinesPresenterOutput] = []
    
    func viewDidLoad() {
        presenter.load()
    }
    
    func selectArticle(at index: Int){
        presenter.select(at: index)
    }
    
    func handleOutput(_ output: TopHeadlinesPresenterOutput) {
        outputs.append(output)
    }
}

private final class MockHeadlinesRouter: TopHeadlinesRouterProtocol {
    
    var routes: [TopHeadlinesRoute] = []
    
    func navigate(to route: TopHeadlinesRoute) {
        routes.append(route)
    }
}
