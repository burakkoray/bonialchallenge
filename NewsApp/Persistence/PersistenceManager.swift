//
//  PersistenceManager.swift
//  Challenge
//
//  Created by Burak Koray Kose on 11.11.2019.
//  Copyright © 2019 Burak Koray Kose. All rights reserved.
//

import Foundation
import CoreData

final class PersistenceManager {
    
    static let sharedManager = PersistenceManager()
    
    private init() {} 
    
    lazy var persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "ArticleDataModel")
        container.loadPersistentStores(completionHandler: { (_, error) in
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    func saveContext () {
        let context = PersistenceManager.sharedManager.persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    func addArticle(article: Article) {
        let managedContext = PersistenceManager.sharedManager.persistentContainer.viewContext
        let entity = NSEntityDescription.entity(forEntityName: "ArticleModel",
                                                in: managedContext)!
        let articleManagedObject = NSManagedObject(entity: entity,
                                                   insertInto: managedContext)
        
        articleManagedObject.setValue(article.title, forKeyPath: "title")
        articleManagedObject.setValue(article.author, forKeyPath: "author")
        articleManagedObject.setValue(article.articleDescription, forKeyPath: "articleDescription")
        articleManagedObject.setValue(article.content, forKeyPath: "content")
        articleManagedObject.setValue(article.url, forKeyPath: "url")
        articleManagedObject.setValue(article.urlToImage, forKeyPath: "urlToImage")
        articleManagedObject.setValue(article.publishedAt, forKeyPath: "publishedAt")
        articleManagedObject.setValue(article.source.name, forKeyPath: "source")
        do {
            try managedContext.save()
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
    }
    
    func addArticleList(articles: [Article]){
        for article in articles
        {
            addArticle(article: article)
        }
    }
    
    func fetchAllArticles() -> [Article]? {
        let managedContext = PersistenceManager.sharedManager.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "ArticleModel")
        
        do {
            let articleModels = try managedContext.fetch(fetchRequest)
            var articleList: [Article] = []
            for articleModel in articleModels{
                articleList.append(convertToModel(from: articleModel as! ArticleModel)!)
            }
            return articleList
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
            return nil
        }
    }
    
    func delete(article: ArticleModel) {
        let managedContext = PersistenceManager.sharedManager.persistentContainer.viewContext
        managedContext.delete(article)
        do {
            try managedContext.save()
        } catch {
            print(error)
        }
    }
    
    func deleteAllArticles() {
        let managedContext = PersistenceManager.sharedManager.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "ArticleModel")
        fetchRequest.returnsObjectsAsFaults = false
        do {
            let results = try managedContext.fetch(fetchRequest)
            for managedObject in results
            {
                if let managedObjectData: NSManagedObject = managedObject as? NSManagedObject {
                    managedContext.delete(managedObjectData)
                }
            }
        } catch let error as NSError {
            print("ArticleModel all data error : \(error) \(error.userInfo)")
        }
    }
    
    func convertToModel(from articleManagedObject: ArticleModel) -> Article? {
        if let sourceName = articleManagedObject.source,
            let author = articleManagedObject.author == nil ? "" : articleManagedObject.author,
            let title = articleManagedObject.title,
            let url = articleManagedObject.url,
            let urlToImage = articleManagedObject.urlToImage,
            let publishedAt = articleManagedObject.publishedAt {
            return Article(source: Source(id: nil, name: sourceName),
                           author: author,
                           title: title,
                           articleDescription: articleManagedObject.articleDescription,
                           url: url,
                           urlToImage: urlToImage,
                           publishedAt: publishedAt,
                           content: articleManagedObject.content)
        }
        return nil
    }
}
