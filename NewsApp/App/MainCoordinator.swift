//
//  MainCoordinator.swift
//  Challenge
//
//  Created by Burak Koray Kose on 9.11.2019.
//  Copyright © 2019 Burak Koray Kose. All rights reserved.
//

import Foundation
import UIKit

class MainCoordinator : Coordinator{
    var childCoordinators = [Coordinator]()
    var navigationController: UINavigationController
    
    lazy var network = NetworkProvider(session: URLSession.shared)
    lazy var headlinesService: NewsServiceProtocol = NewsService(network: network) as NewsServiceProtocol
    
    init(navigationController : UINavigationController) {
        self.navigationController = navigationController
    }
    
    func start() {
        let view = TopHeadlinesViewController.instantiate()
        let interactor = TopHeadlinesInteractor(service: headlinesService)
        let router = TopHeadlinesRouter(view: view)
        let presenter = TopHeadlinesPresenter(view: view,
                                              interactor: interactor,
                                              router: router)
        view.presenter = presenter
        view.coordinator = self
        
        navigationController.pushViewController(view, animated: false)
    }
    
    func showArticleDetail(article : Article){
        let view = ArticleDetailViewController.instantiate()
        view.coordinator = self
        let interactor = ArticleDetailInteractor()
        let router = ArticleDetailRouter(view: view)
        let presenter = ArticleDetailPresenter(view: view, article: article, interactor: interactor, router: router)
        view.presenter = presenter
        navigationController.pushViewController(view, animated: true)
    }
    
    func openWebview(url: String, pageTitle: String){
        let view = WebViewController.instantiate()
        view.urlString = url
        view.pageTitle = pageTitle
        navigationController.pushViewController(view, animated: true)
    }
}
