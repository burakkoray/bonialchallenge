//
//  ArticleViewCell.swift
//  Challenge
//
//  Created by Burak Koray Kose on 9.11.2019.
//  Copyright © 2019 Burak Koray Kose. All rights reserved.
//

import UIKit

class ArticleViewCell: UICollectionViewCell {
    @IBOutlet weak var articleImageView: UIImageView!
    @IBOutlet weak var articleTitleLabel: UILabel!
    @IBOutlet weak var sourceLabel: UILabel!
    @IBOutlet weak var articleDescriptionLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func prepareForReuse() {
         super.prepareForReuse()
         articleImageView.image = nil
         articleTitleLabel.text = nil
         sourceLabel.text = nil
         articleDescriptionLabel.text = nil
     }
}
