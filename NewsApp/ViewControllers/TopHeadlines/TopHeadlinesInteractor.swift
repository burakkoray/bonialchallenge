//
//  TopHeadlinesInteractor.swift
//  Challenge
//
//  Created by Burak Koray Kose on 9.11.2019.
//  Copyright © 2019 Burak Koray Kose. All rights reserved.
//

import Foundation


final class TopHeadlinesInteractor: TopHeadlinesInteractorProtocol {
    
    weak var delegate: TopHeadlinesInteractorDelegate?
    
    private let service: NewsServiceProtocol
    private var articles: [Article] = []
    private var currentPage = 1
    private var totalArticleCount = 0
    
    init(service: NewsServiceProtocol) {
        self.service = service
    }
    
    func load() {
        delegate?.handleOutput(.setLoading(true))
        
        if(Reachability.isConnectedToNetwork()){
            service.getTopHeadlines(parameters: ["country": "de", "pageSize" : Constants.ApiConstants.PAGE_SIZE, "page" : "1"])
              { [weak self] (result) in
                  guard let self = self else { return }
                  self.delegate?.handleOutput(.setLoading(false))
                  
                  switch result {
                  case .success(let response):
                      self.articles = response.articles
                      self.totalArticleCount = response.totalResults
                      self.delegate?.handleOutput(.showHeadlineList(response.articles))
                      PersistenceManager.sharedManager.deleteAllArticles()
                      PersistenceManager.sharedManager.addArticleList(articles: response.articles)
                  case .failure(let error):
                      print(error)
                  }
              }
        }else{
            delegate?.handleOutput(.setLoading(false))
            if let articles = PersistenceManager.sharedManager.fetchAllArticles() {
                self.totalArticleCount = articles.count
                self.articles = articles
                self.delegate?.handleOutput(.showHeadlineList(articles))
            }
        }
    }
    
    func loadMoreHeadlines() {
        if(articles.count != totalArticleCount) {
            currentPage += 1
            delegate?.handleOutput(.setLoading(true))
            
            service.getTopHeadlines(parameters: ["country": "de", "page":String(currentPage), "pageSize" : Constants.ApiConstants.PAGE_SIZE] ) { [weak self] (result) in
                guard let self = self else { return }
                self.delegate?.handleOutput(.setLoading(false))
                
                switch result {
                case .success(let response):
                    self.articles.append(contentsOf: response.articles)
                    self.delegate?.handleOutput(.loadMoreHeadline(response.articles))
                    PersistenceManager.sharedManager.addArticleList(articles: response.articles)
                case .failure(let error):
                    print(error)
                }
            }
        }
    }
    
    func selectArticle(at index: Int) {
        let article = articles[index]
        delegate?.handleOutput(.showHeadlineDetail(article))
    }
}
