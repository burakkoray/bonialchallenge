//
//  TopHeadlinesRouter.swift
//  Challenge
//
//  Created by Burak Koray Kose on 9.11.2019.
//  Copyright © 2019 Burak Koray Kose. All rights reserved.
//

import Foundation
import UIKit

final class TopHeadlinesRouter: TopHeadlinesRouterProtocol {
    
    unowned let view: TopHeadlinesViewController
    
    init(view: TopHeadlinesViewController) {
        self.view = view
    }
    
    func navigate(to route: TopHeadlinesRoute) {
        switch route {
        case .detail(let article):
            view.coordinator?.showArticleDetail(article: article)
        }
    }
}
