//
//  TopHeadlinesContracts.swift
//  Challenge
//
//  Created by Burak Koray Kose on 9.11.2019.
//  Copyright © 2019 Burak Koray Kose. All rights reserved.
//

import Foundation

// MARK: - Interactor

protocol TopHeadlinesInteractorProtocol: class {
    var delegate: TopHeadlinesInteractorDelegate? { get set }
    func load()
    func loadMoreHeadlines()
    func selectArticle(at index: Int)
}

enum TopHeadlinesInteractorOutput: Equatable {
    case setLoading(Bool)
    case showHeadlineList([Article])
    case loadMoreHeadline([Article])
    case showHeadlineDetail(Article)
}

protocol TopHeadlinesInteractorDelegate: class {
    func handleOutput(_ output: TopHeadlinesInteractorOutput)
}

// MARK: - Presenter

protocol TopHeadlinesPresenterProtocol: class {
    func load()
    func loadMoreHeadlines()
    func select(at index: Int)
}

enum TopHeadlinesPresenterOutput: Equatable {
    case setLoading(Bool)
    case showArticleList([ArticlePresentation])
    case loadMoreHeadlines([ArticlePresentation])
}

// MARK: - View

protocol TopHeadlinesViewProtocol: class {
    func handleOutput(_ output: TopHeadlinesPresenterOutput)
}

// MARK: - Router

enum TopHeadlinesRoute: Equatable {
    case detail(Article)
}

protocol TopHeadlinesRouterProtocol: class {
    func navigate(to route: TopHeadlinesRoute)
}
