//
//  TopHeadlinesPresenter.swift
//  Challenge
//
//  Created by Burak Koray Kose on 9.11.2019.
//  Copyright © 2019 Burak Koray Kose. All rights reserved.
//

import Foundation

final class TopHeadlinesPresenter: TopHeadlinesPresenterProtocol {
    
    private unowned let view: TopHeadlinesViewProtocol
    private let interactor: TopHeadlinesInteractorProtocol
    private let router: TopHeadlinesRouterProtocol
    
    init(view: TopHeadlinesViewProtocol,
         interactor: TopHeadlinesInteractorProtocol,
         router: TopHeadlinesRouterProtocol) {
        self.view = view
        self.interactor = interactor
        self.router = router
        
        self.interactor.delegate = self
    }
    
    func load() {
        interactor.load()
    }
    
    func loadMoreHeadlines() {
        interactor.loadMoreHeadlines()
    }
    
    func select(at index: Int) {
        interactor.selectArticle(at: index)
    }
}

extension TopHeadlinesPresenter: TopHeadlinesInteractorDelegate {
    func handleOutput(_ output: TopHeadlinesInteractorOutput) {
        switch output {
        case .setLoading(let isLoading):
            view.handleOutput(.setLoading(isLoading))
        case .showHeadlineList(let articles):
            let articlePresentations = articles.map(ArticlePresentation.init)
            view.handleOutput(.showArticleList(articlePresentations))
        case .showHeadlineDetail(let article):
            router.navigate(to: .detail(article))
        case .loadMoreHeadline(let articles):
            let articlePresentations = articles.map(ArticlePresentation.init)
            view.handleOutput(.loadMoreHeadlines(articlePresentations))
        }
    }
}
