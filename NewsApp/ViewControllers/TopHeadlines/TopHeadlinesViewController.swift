//
//  TopHeadlinesViewController.swift
//  Challenge
//
//  Created by Burak Koray Kose on 9.11.2019.
//  Copyright © 2019 Burak Koray Kose. All rights reserved.
//

import UIKit
import Kingfisher

class TopHeadlinesViewController: UIViewController, Storyboarded, TopHeadlinesViewProtocol {
    
    @IBOutlet weak var headlinesCollectionView: UICollectionView!
    
    weak var coordinator : MainCoordinator?
    var presenter: TopHeadlinesPresenterProtocol!
    private var articles: [ArticlePresentation] = []
    var cellsPerRow:CGFloat = Constants.ViewConstants.itemPerRowInPortrait
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.load()
        setupView()
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        
        if UIDevice.current.orientation.isLandscape {
            cellsPerRow = Constants.ViewConstants.itemPerRowInLandscape
        } else {
            cellsPerRow = Constants.ViewConstants.itemPerRowInPortrait
        }
        
        guard let flowLayout = self.headlinesCollectionView.collectionViewLayout as? UICollectionViewFlowLayout else {
            return
        }
        flowLayout.invalidateLayout()
        
        self.headlinesCollectionView.reloadData()
    }
    
    private func setupView() {
        headlinesCollectionView.delegate = self as UICollectionViewDelegate
        headlinesCollectionView.dataSource = self as UICollectionViewDataSource
        title = "title".localized
    }
    
    func handleOutput(_ output: TopHeadlinesPresenterOutput) {
        switch output {
        case .setLoading(let isLoading):
            DispatchQueue.main.async {
                UIApplication.shared.isNetworkActivityIndicatorVisible = isLoading
            }
        case .showArticleList(let articles):
            self.articles = articles
            DispatchQueue.main.async {
                self.headlinesCollectionView.reloadData()
            }
        case .loadMoreHeadlines(let articles):
            self.articles.append(contentsOf: articles)
            DispatchQueue.main.async {
                self.headlinesCollectionView.reloadData()
            }
        }
    }
}

extension TopHeadlinesViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        self.articles.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ArticleViewCell",
                                                            for: indexPath) as? ArticleViewCell else {
                                                                fatalError("Cell is not found")
        }
        
        let article = articles[indexPath.row]
        
        cell.articleTitleLabel.text = article.title
        
        cell.articleImageView.kf.setImage(with: URL(string: article.urlToImage))
        
        cell.articleImageView.kf.indicatorType = .activity
        
        cell.articleImageView.kf.setImage(
            with: URL(string: article.urlToImage),
            placeholder: UIImage(named: "placeholderImage"),
            options: [
                .transition(.fade(1)),
                .cacheOriginalImage
        ])
        
        cell.articleDescriptionLabel.text = article.articleDescription
        cell.sourceLabel.text = article.source.name
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        let lastElement = articles.count - 1
        if indexPath.row == lastElement {
            presenter.loadMoreHeadlines()
        }
    }
}

extension TopHeadlinesViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        presenter.select(at: indexPath.row)
    }
}

extension TopHeadlinesViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if indexPath.row % Constants.ViewConstants.fullWidthItemRepeatCount == 0 {
            let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout
            let unavailableSpace = flowLayout.sectionInset.left
                + flowLayout.sectionInset.right
                + (flowLayout.minimumInteritemSpacing ) + 20
            
            let size = Int((collectionView.bounds.width - unavailableSpace))
            
            return CGSize(width: size, height: Constants.ViewConstants.cellHeight)
            
        } else {
            let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout
            let unavailableSpace = flowLayout.sectionInset.left
                + flowLayout.sectionInset.right
                + (flowLayout.minimumInteritemSpacing * CGFloat(cellsPerRow - 1)) + 30
            
            let size = Int((collectionView.bounds.width - unavailableSpace) / CGFloat(cellsPerRow))
            
            return CGSize(width: size, height: Constants.ViewConstants.cellHeight)
        }
    }
}
