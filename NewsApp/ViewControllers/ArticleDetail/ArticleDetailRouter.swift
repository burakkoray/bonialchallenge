//
//  ArticleDetailRouter.swift
//  Challenge
//
//  Created by Burak Koray Kose on 9.11.2019.
//  Copyright © 2019 Burak Koray Kose. All rights reserved.
//

import Foundation
import UIKit

final class ArticleDetailRouter: ArticleDetailRouterProtocol {
    
    unowned let view: ArticleDetailViewController
    
    init(view: ArticleDetailViewController) {
        self.view = view
    }
    
    func navigate(to route: ArticleDetailRoute) {
        switch route {
        case .webView(let url, let pageTitle):
            view.coordinator?.openWebview(url: url, pageTitle: pageTitle)
        }        
    }
}
