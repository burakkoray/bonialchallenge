//
//  ArticleDetailContracts.swift
//  Challenge
//
//  Created by Burak Koray Kose on 9.11.2019.
//  Copyright © 2019 Burak Koray Kose. All rights reserved.
//

import Foundation

// MARK: - Interactor
protocol ArticleDetailInteractorProtocol: class {
    var delegate: ArticleDetailInteractorDelegate? { get set }
    func readMore()
}

enum ArticleDetailInteractorOutput: Equatable {
    case openWebview
}

protocol ArticleDetailInteractorDelegate: class {
    func handleOutput(_ output: ArticleDetailInteractorOutput)
}

// Presenter
protocol ArticleDetailPresenterProtocol {
    func load()
    func readMore()
}


// View
protocol ArticleDetailViewProtocol: class {
    func update(_ presentation: ArticleDetailPresentation)
}

// MARK: - Router
enum ArticleDetailRoute: Equatable {
    case webView(String,String)
}

protocol ArticleDetailRouterProtocol: class {
    func navigate(to route: ArticleDetailRoute)
}
