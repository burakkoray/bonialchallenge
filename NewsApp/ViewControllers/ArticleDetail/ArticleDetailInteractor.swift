//
//  ArticleDetailInteractor.swift
//  Challenge
//
//  Created by Burak Koray Kose on 9.11.2019.
//  Copyright © 2019 Burak Koray Kose. All rights reserved.
//

import Foundation

final class ArticleDetailInteractor: ArticleDetailInteractorProtocol {
    
    weak var delegate: ArticleDetailInteractorDelegate?
    
    func readMore() {
        self.delegate?.handleOutput(.openWebview)
    }
}
