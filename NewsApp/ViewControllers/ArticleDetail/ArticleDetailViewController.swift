//
//  ArticleDetailViewController.swift
//  Challenge
//
//  Created by Burak Koray Kose on 9.11.2019.
//  Copyright © 2019 Burak Koray Kose. All rights reserved.
//

import UIKit
import Kingfisher

final class ArticleDetailViewController: UIViewController, ArticleDetailViewProtocol, Storyboarded {
    
    var presenter: ArticleDetailPresenterProtocol!
    weak var coordinator : MainCoordinator?
    
    @IBOutlet weak var articleImageView: UIImageView!
    @IBOutlet weak var articleContentLabel: UILabel!
    @IBOutlet weak var articleSourceLabel: UILabel!
    @IBOutlet weak var articleAuthorAndPublishedDateInfoLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.load()
    }
    
    @IBAction func readFullArticleClicked(_ sender: Any) {
        presenter.readMore()
    }
    
    func update(_ presentation: ArticleDetailPresentation) {
        articleImageView.kf.setImage(
            with: URL(string: presentation.urlToImage),
            placeholder: UIImage(named: "placeholderImage"),
            options: [
                .transition(.fade(1)),
                .cacheOriginalImage
        ])
        articleContentLabel.text = presentation.content
        articleSourceLabel.text = presentation.source.name
        articleAuthorAndPublishedDateInfoLabel.text = "\(presentation.author ?? "")  \(presentation.publishedAt.toString())"
        title = presentation.title
    }
}
