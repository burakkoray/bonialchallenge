//
//  ArticleDetailPresenter.swift
//  Challenge
//
//  Created by Burak Koray Kose on 9.11.2019.
//  Copyright © 2019 Burak Koray Kose. All rights reserved.
//

import Foundation

final class ArticleDetailPresenter: ArticleDetailPresenterProtocol {
    
    unowned var view: ArticleDetailViewProtocol
    private let article: Article
    private let interactor: ArticleDetailInteractorProtocol
    private let router: ArticleDetailRouterProtocol
    
    init(view: ArticleDetailViewProtocol, article: Article, interactor: ArticleDetailInteractorProtocol,
         router: ArticleDetailRouterProtocol) {
        self.view = view
        self.interactor = interactor
        self.router = router
        self.article = article
        
        self.interactor.delegate = self
    }
    
    func load() {
        view.update(ArticleDetailPresentation(article: article))
    }
    
    func readMore() {
        interactor.readMore()
    }
}

extension ArticleDetailPresenter: ArticleDetailInteractorDelegate {
    func handleOutput(_ output: ArticleDetailInteractorOutput) {
        switch output {
        case .openWebview:
            router.navigate(to: .webView(article.url,article.title))
        }
    }
}
