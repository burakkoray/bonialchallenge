//
//  WebViewController.swift
//  Challenge
//
//  Created by Burak Koray Kose on 11.11.2019.
//  Copyright © 2019 Burak Koray Kose. All rights reserved.
//

import UIKit
import WebKit

class WebViewController: UIViewController, Storyboarded{
    
    var webView: WKWebView!
    var urlString: String = ""
    var pageTitle: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let url = URL(string: urlString)!
        webView.load(URLRequest(url: url))
        self.title = pageTitle
    }
    
    override func loadView() {
        webView = WKWebView()
        view = webView
    }
}
