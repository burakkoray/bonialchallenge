//
//  NetworkProvider.swift
//  Challenge
//
//  Created by Burak Koray Kose on 9.11.2019.
//  Copyright © 2019 Burak Koray Kose. All rights reserved.
//

import Foundation

class NetworkProvider: Network {
   
    let session: URLSession
    
    init(session: URLSession = URLSession.shared) {
        self.session = session
    }
    
    public func makeRequest(request: NetworkRequest, callback: @escaping (Result<Data,NetworkError>) -> Void) {
        do {
            let request = try request.buildURLRequest()
            let task = self.session.dataTask(with: request) { (data, _, error) in
                guard let data = data else {
                    DispatchQueue.main.async {
                        callback(.failure(NetworkError.unknown))
                    }
                    return
                }
                callback(.success(data))
            }
            task.resume()
        } catch {
            callback(.failure(NetworkError.unknown))
        }
    }
}
