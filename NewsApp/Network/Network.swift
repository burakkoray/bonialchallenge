//
//  Network.swift
//  Challenge
//
//  Created by Burak Koray Kose on 9.11.2019.
//  Copyright © 2019 Burak Koray Kose. All rights reserved.
//

import Foundation

public enum NetworkError: Error {
    case unknown
    case invalidResponse
    case serializationError(internal : Error)
    case networkError(internal : Error)
}

protocol Network {
    func makeRequest(request: NetworkRequest, callback: @escaping (Result<Data,NetworkError>) -> Void)
}
