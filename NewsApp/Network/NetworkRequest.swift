//
//  NetworkRequest.swift
//  Challenge
//
//  Created by Burak Koray Kose on 9.11.2019.
//  Copyright © 2019 Burak Koray Kose. All rights reserved.
//

import Foundation

enum NetworkRequestError: Error, LocalizedError {
    case invalidURL(String)
    public var errorDescription: String? {
        switch self {
        case .invalidURL(let url):
            return NSLocalizedString("The url '\(url)' was invalid", comment: "My error")
        }
    }
}

struct NetworkRequest {
    enum Method: String {
        case get        = "GET"
        case put        = "PUT"
        case patch      = "PATCH"
        case post       = "POST"
        case delete     = "DELETE"
    }
    
    let method: NetworkRequest.Method
    let url: String
    let parameters: [String: String]
    
    func buildURLRequest() throws -> URLRequest {
        
        var components = URLComponents(string: url)!
        components.queryItems = parameters.map { (key, value) in
            URLQueryItem(name: key, value: value)
        }
        components.queryItems?.append(URLQueryItem(name: "apiKey", value: Constants.ApiConstants.API_KEY))
        
        components.percentEncodedQuery = components.percentEncodedQuery?.replacingOccurrences(of: "+", with: "%2B")
        
        guard let url = URL(string: components.url!.absoluteString) else {
            throw NetworkRequestError.invalidURL(components.url?.absoluteString ?? "") }
        
        var request = URLRequest(url: url)
        request.httpMethod = self.method.rawValue
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        
        return request
    }
}
