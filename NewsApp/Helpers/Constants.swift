//
//  Constants.swift
//  Challenge
//
//  Created by Burak Koray Kose on 9.11.2019.
//  Copyright © 2019 Burak Koray Kose. All rights reserved.
//

import Foundation
import UIKit

class Constants: NSObject {
    struct ApiConstants {
        static let BASE_URL = "https://newsapi.org/v2/top-headlines?"
        static let API_KEY = "5b108408924c4310a2d9b7339578daa7"
        static let PAGE_SIZE = "21"
    }
    
    struct ViewConstants {
        static let itemPerRowInPortrait: CGFloat = 2
        static let itemPerRowInLandscape: CGFloat = 3
        static let fullWidthItemRepeatCount: Int = 7
        static let cellHeight:Int = 220
    }
}
