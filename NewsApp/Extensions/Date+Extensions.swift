//
//  Date+Extensions.swift
//  Challenge
//
//  Created by Burak Koray Kose on 10.11.2019.
//  Copyright © 2019 Burak Koray Kose. All rights reserved.
//

import Foundation

extension Date {
    func toString(format: String = "yyyy-MM-dd") -> String {
        let formatter = DateFormatter()
        formatter.dateStyle = .short
        formatter.dateFormat = format
        return formatter.string(from: self)
    }
}
