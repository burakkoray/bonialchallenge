//
//  String+Extensions.swift
//  Challenge
//
//  Created by Burak Koray Kose on 11.11.2019.
//  Copyright © 2019 Burak Koray Kose. All rights reserved.
//

import Foundation

extension String {
    var localized: String {
        return NSLocalizedString(self, comment: "")
    }
}
