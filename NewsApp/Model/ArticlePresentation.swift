//
//  ArticlePresentation.swift
//  Challenge
//
//  Created by Burak Koray Kose on 9.11.2019.
//  Copyright © 2019 Burak Koray Kose. All rights reserved.
//

import Foundation

final class ArticlePresentation: NSObject {
    
    let source: Source
    let author: String?
    let title, articleDescription: String
    let url: String
    let urlToImage: String
    let publishedAt: Date
    let content: String?
    
    init(source: Source, author: String, title: String, articleDescription: String, url : String,
         urlToImage: String, publishedAt: Date, content: String) {
        self.source = source
        self.author = author
        self.title = title
        self.articleDescription = articleDescription
        self.url = url
        self.urlToImage = urlToImage
        self.publishedAt = publishedAt
        self.content = content
        
        super.init()
    }
    
    override func isEqual(_ object: Any?) -> Bool {
        guard let other = object as? ArticlePresentation else { return false }
        return self.source.id == other.source.id
    }
}


extension ArticlePresentation {
    convenience init(article: Article) {
        self.init(source: article.source, author: article.author ?? "", title: article.title,
                  articleDescription: article.articleDescription ?? "",url: article.url,
                  urlToImage: article.urlToImage, publishedAt: article.publishedAt, content: article.content ?? "")
    }
}

