//
//  ResourceLoad.swift
//  Challenge
//
//  Created by Burak Koray Kose on 9.11.2019.
//  Copyright © 2019 Burak Koray Kose. All rights reserved.
//

import Foundation

class ResourceLoader {
    public static func loadHeadlines() throws -> TopHeadlines {
        let bundle = Bundle.main
        let url = try bundle.url(forResource: "headlinelist", withExtension: "json")
        let data = try Data(contentsOf: url!)
        let decoder = Decoders.plainDateDecoder
        let templates = try decoder.decode(TopHeadlines.self, from: data)
        return templates
    }
}

private extension Bundle {
    class Dummy { }
    static let test = Bundle(for: Dummy.self)
}
