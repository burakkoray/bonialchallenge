//
//  MockNewsService.swift
//  Challenge
//
//  Created by Burak Koray Kose on 9.11.2019.
//  Copyright © 2019 Burak Koray Kose. All rights reserved.
//

import Foundation

final class MockNewsService: NewsServiceProtocol {
    
    var topHeadlines:TopHeadlines?
    
    func getTopHeadlines(parameters: [String : String],
                         completion: @escaping (Result<TopHeadlines, NetworkError>) -> Void) {
        completion(.success(TopHeadlines(status: topHeadlines!.status,
                                         totalResults: topHeadlines!.totalResults,
                                         articles: topHeadlines!.articles)))
    }
}
