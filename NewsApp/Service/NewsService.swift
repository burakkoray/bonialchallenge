//
//  NewsService.swift
//  Challenge
//
//  Created by Burak Koray Kose on 9.11.2019.
//  Copyright © 2019 Burak Koray Kose. All rights reserved.
//

import Foundation

public protocol NewsServiceProtocol {
    func getTopHeadlines(parameters: [String: String], completion: @escaping (Result<TopHeadlines,NetworkError>) -> Void)
}

class NewsService: NewsServiceProtocol{
    private let network: Network
    
    init(network: Network) {
        self.network = network
    }
    
    func getTopHeadlines(parameters: [String: String], completion: @escaping (Result<TopHeadlines,NetworkError>) -> Void) {
        let request = NetworkRequest(
            method: .get,
            url: Constants.ApiConstants.BASE_URL,
            parameters: parameters
        )
        _ = network.makeRequest(request: request, callback: {  result in
            switch result {
            case .success(let data):
                do {
                    let decoder = Decoders.plainDateDecoder
                    let topheadlines : TopHeadlines = try decoder.decode(TopHeadlines.self, from: data)
                    completion(.success(topheadlines))
                    
                } catch let error {
                    completion(.failure(NetworkError.serializationError(internal: error)))
                }
            case .failure(let error):
                completion(.failure(NetworkError.networkError(internal: error)))
                break
            }
        })
    }
}

